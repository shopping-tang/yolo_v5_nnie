# yolo_v5_nnie

#### 介绍
yolo_v5 nnie 推理，相关解释博客：https://blog.csdn.net/tangshopping/article/details/110038605 ，有些细节我在博客里做了说明及阐述了自己的见解。

#### 软件架构
AI框架什么的，nnie已经搭建好了，该 demo 更像是 yolo v5的后处理部分


#### 安装教程

无

#### 使用说明

1.  由于某些原因，不方便把整个项目发进来，只取精华部分—— yolo v5 后处理；
2.  如果想要完整项目版本的，可参考我的另一个仓库（SSD 行人检测那个），那里面有海思一些库及源文件，移植一下就好了；
3.  该 demo 只有图片推理，视频推理参考我的另一个仓库（SSD 行人检测那个），二者架构一致，替换二者的后处理函数就好；
4.  三个源文件，一个主函数所在文件，一个配置文件（anchor 尺寸之类的），一个后处理文件（后处理 api 基本在这里），个人觉得主要看最后一个文件，次看中间那个文件；
5.  仓库有点简陋，但是都是最核心的部分，其他那些海思相关库、源文件之类的，没什么用，不放出干扰大家视线了；
6.  水平有限，如有错误，请告知我，谢谢。水平有限，一些函数 api 可能没写好，如大神有更佳实现方式，请告知我，我学习一下！谢谢；
7.  推理时间没测试，据群友们说，yolo v5s 在 3516D V300 下单帧推理时间为 40ms 左右（320x320输入）；
8.  特意放出我的 prototxt 文件，用该 demo 之前，查看下我的输出 shape ，免得出现错误。
#### 参与贡献

无


#### 特技

无
