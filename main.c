#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include "vision/nnie_face_api.h"
#include <assert.h>
#include <math.h>

int main(int argc, char* argv[]){
	char *pcSrcFile = "./data/lanmei_.bgr";
	if (argc != 2){
		printf("parameter num is wrong \n");
	}else{
		pcSrcFile = argv[1];
	}
    char *pcModelName = "./data/fruit_no_permute.wk";
    //char *pcSrcFile = "./data/lanmei_.bgr";
    
    float iou_threshold = 0.5f;
	float conf_threshold = 0.4f;
    printf("pcSrcFile = %s\n", pcSrcFile);
    int ret = yolo_image_inference(pcModelName, conf_threshold, iou_threshold, pcSrcFile);
}


